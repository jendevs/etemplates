# Email Templates, Layouts, Partials and Code Snippets

This repo contains various example email templates, layouts and partials. Also there are various email code snippets designed to help create emails.

Please label the correct folders according to the email lesson and design, to help searching for the correct files and assets in the future.